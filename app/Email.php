<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Email extends Model
{
    protected $fillable = [
        'nama','date', 'address', 'line','ktp', 'phone', 'email', 'git', 'provience','role','user_id'
    ];
    public function user(){
       return  $this->belongsTo('App\User','id');
    }
    public function uploaduser(){
      return  $this->hasOne('App\Uploaduser');
    }

}
