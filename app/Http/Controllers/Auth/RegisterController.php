<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Email;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use App\Mail\RegisterMail;
use Illuminate\Support\Facades\Mail;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = 'login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {

        Validator::extend('without_spaces', function($attr, $value){
            return preg_match('/^\S*$/u', $value);
        });
        return $validator = Validator::make($data, [



        ]);


    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $request =Request();
        $this->validate($request,[
            'nama.*' => ['required','string'],
            'email.*' => ['required','email'],
            'date.*'=> ['required'],
            'ktp.*' =>['required','mimes:pdf,jpeg'],
            'phone.*' => ['required','unique:emails,phone'],
            'address.*' => ['required', 'string'],
            'git.*' => ['required','unique:emails,git'],
            'provience.*' =>['required'],
            'group' => ['required','unique:users,group','regex:/^\S*$/u'],
            'password.*' => ['required', 'string', 'min:8', 'confirmed'],
            'line.*'=>['required'],
        ]);
        $request= Request();
        $users = $request -> all();


        $path = $request->file('ktp')->store('ktp');
        $path1 = $request->file('ktp1')->store('ktp');
        $path2 = $request->file('ktp2')->store('ktp');

        User::create([
            // $nama = 'nama', $email = 'email' , $date = 'date' , $address = 'address',
            'group'=>$request->group,
            'password'=>bcrypt($request->password),

            ]);

            $id = User::all()->last()->id;
        // $id= User::find($user);
        // $email =Email::where('id','=',$id);

        Email::create([
            'nama'=>$request->nama,
            'email'=>$data['email'],
            'date'=>$data['date'],
            'address'=>$data['address'],
            'phone'=>$data['phone'],
            'git'=>$data['git'],
            'provience'=>$data['provience'],
            'line'=>$data['line'],
            'role'=>'Leader',
            'ktp'=>$path,
            'user_id'=>$id,
        ]);
        Email::create([
            'nama'=>$request->nama1,
            'email'=>$data['email1'],
            'date'=>$data['date1'],
            'address'=>$data['address1'],
            'phone'=>$data['phone1'],
            'git'=>$data['git1'],
            'provience'=>$data['provience1'],
            'line'=>$data['line1'],
            'role'=>'member1',
            'ktp'=>$path1,
            'user_id'=>$id,
        ]);
        Email::create([
            'nama'=>$request->nama2,
            'email'=>$data['email2'],
            'date'=>$data['date2'],
            'address'=>$data['address2'],
            'phone'=>$data['phone2'],
            'git'=>$data['git2'],
            'provience'=>$data['provience2'],
            'line'=>$data['line2'],
            'role'=>'member2',
            'ktp'=>$path2,
            'user_id'=>$id,
        ]);

        Mail::to('test@gmail.com')->send(new RegisterMail($data));

        return view('auth.login');
    }

}
