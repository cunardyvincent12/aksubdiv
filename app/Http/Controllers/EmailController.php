<?php

namespace App\Http\Controllers;

use App\Email;
use App\User;
use App\Uploaduser;
use App\Payment;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class EmailController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = Auth::user();
        $payments = Payment::all();
        $id = $users->id;
        $emails = User::find($id)->emails;
        // dd($emails);
        $files = $users->with('payment')->get();
        // $emails->user_id =Auth::user()->id;
        $uploadusers =Uploaduser::all();
        // return view('home',compact('emails','uploadusers'));
            if (Auth::user()-> admin == 0){
                return view('home',compact('users','emails','uploadusers','payments'));
            }else{
                $users['users'] = User::all();
                Storage::put('file.jpg', $files, 'public');
                $error = 0;
                return view('admin.confirmation',$users,compact('error','files'));
            }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\email  $email
     * @return \Illuminate\Http\Response
     */
    public function show(email $email)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\email  $email
     * @return \Illuminate\Http\Response
     */
    public function edit(email $email)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\email  $email
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, email $email)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\email  $email
     * @return \Illuminate\Http\Response
     */
    public function destroy(email $email)
    {
        //
    }
}
