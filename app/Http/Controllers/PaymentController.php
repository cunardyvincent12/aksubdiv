<?php

namespace App\Http\Controllers;

use App\payment;
use App\User;
use Illuminate\Support\Facades\Storage;
use Auth;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = Auth::user();
        $id = $users->id;
        $temps = Payment::where('user_id',$id)->first();
        if (!empty($temps)) {

        $payments = Payment::find($temps->id);
        if(!empty($payments)){
            if($payments->paid == 1){
            $stat = 1;
            }
            else{
                $stat = 0;
            }
        }
        else{
                $stat = 0;
        }
        }
        else{
            $stat = 0;
        }

        return view('/payment',compact('stat','users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $id = Auth::user()->id;
        $temps = Payment::where('user_id',$id)->first();
        // $payments = Payment::find($temps->id);
        $this->validate($request,[
            'receipt'=>'required',
        ]);
        $path= $request->file('receipt')->store('receipt');
        // dd($payments);
        if(empty($temps)){
            Payment::create([
            'receipt'=>$path,
            'paid'=>0,
            'user_id'=>$id,
        ]);
    }
    else{
        // $id = Auth::user()->id;
        // dd($payments);
        // $payments = new payment();
        $temps->receipt = $path;
        $temps->user_id = $id;
        $temps->paid = 0;
        $temps->save();
    }
        return redirect('/payment');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function show(payment $payment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function edit(payment $payment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        // $id = Auth::User()->id;
        $upload= Payment::find($id);

        $path= $request->file('receipt')->store('receipt');
        $upload->receipt = $request->$path;
        $upload->user_id = $request->$id;
        $upload->save();
        return redirect('/payment');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function destroy(payment $payment)
    {
        //
    }

    public function pay(Request $request){
        $id = $request->id;
        $bolpay = Payment::where('user_id',$id)->first();
        // dd($bolpay->paid);
        if(empty($bolpay)){

        }
        else if($bolpay->paid == 0){
            $bolpay->paid = 1;
            $bolpay->save();
        }
        return redirect('/home');
    }
    public function check(Request $request){
        $users = Auth::user();
        $payments = Payment::all();
        $files = $users->with('payment')->get();
        // $emails->user_id =Auth::user()->id;
        $files = $users->with('payment')->get();
        $users['users'] = User::all();
                Storage::put('file.jpg', $files, 'public');
                $error = 1;
                return view('admin.confirmation',$users,compact('error'));
    }
}
