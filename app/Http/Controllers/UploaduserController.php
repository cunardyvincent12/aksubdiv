<?php

namespace App\Http\Controllers;

use App\Uploaduser;
use App\Email;
use Auth;
use Illuminate\Http\Request;

class UploaduserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'cv.*'=>'required|mimes:jpeg,pdf',
        ]);
        $id = $request->id;
        $uploads = Email::find($id)->uploaduser;
        $emails = Email::all();
        $path=$request->file('cv0')->store('cv');

        if(empty($uploads)){
        UploadUser::create([
            'cv'=>$path,
            'email_id'=>$id
        ]);
        }
        else{
            $uploads->cv = $path;
            $uploads->id = $uploads->id;
            $uploads->save();
        }
        $users = Auth::user();
        $emails->user_id =Auth::user()->id;
        $uploadusers =Uploaduser::all();
        // return view('home',compact('emails','uploadusers','users','uploads'));
        return redirect('/home');
    }public function store1(Request $request)
    {
        $data = $this->validate($request,[
            'cv.*'=>'required|mimes:jpeg,pdf',
        ]);
        $emails = Email::all();
        $path1=$request->file('cv1')->store('cv');
        $id = $request->id;
        $uploads = Uploaduser::where('email_id',$id)->first();
        if(empty($uploads)){
        UploadUser::create([
            'cv'=>$path1,
            'email_id'=>$id
        ]);
        }
        else{
            $uploads->cv = $path1;
            $uploads->id = $uploads->id;
            $uploads->save();
        } $users = Auth::user();
        $emails->user_id =Auth::user()->id;
        $uploadusers =Uploaduser::all();
        // return view('home',compact('emails','uploadusers','users'));
        return redirect('/home');

    }
    public function store2(Request $request)
    {
        $data = $this->validate($request,[
            'cv.*'=>'required|mimes:jpeg,pdf',
        ]);
        $emails = Email::all();
        $path2=$request->file('cv2')->store('cv');
        $id = $request->id;
        $uploads = Uploaduser::where('email_id',$id)->first();
        if(empty($uploads)){
        UploadUser::create([
            'cv'=>$path2,
            'email_id'=>$id
        ]);
        }
        else{
            $uploads->cv = $path2;
            $uploads->id = $uploads->id;
            $uploads->save();
        }
        $users = Auth::user();
        $emails->user_id =Auth::user()->id;
        $uploadusers =Uploaduser::all();
        // return view('home',compact('emails','uploadusers','users'));
        return redirect('/home');
    }

//     $id = Auth::user()->id;
//     $temps = Payment::where('user_id',$id)->first();
//     $uploads = Payment::find($temps->id);
//     $this->validate($request,[
//         'receipt'=>'required',
//     ]);
//     $path= $request->file('receipt')->store('receipt');
//     // dd($uploads);
//     if(empty($uploads)){
//         Payment::create([
//         'receipt'=>$path,
//         'paid'=>0,
//         'user_id'=>$id,
//     ]);
// }
// else{
//     // $id = Auth::user()->id;
//     // dd($uploads);
//     // $uploads = new payment();
//     $uploads->receipt = $path;
//     $uploads->user_id = $id;
//     $uploads->paid = 0;
//     $uploads->save();
// }
//     return redirect('/payment');
    /**
     * Display the specified resource.
     *
     * @param  \App\Uploaduser  $uploaduser
     * @return \Illuminate\Http\Response
     */
    public function show(Uploaduser $uploaduser)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Uploaduser  $uploaduser
     * @return \Illuminate\Http\Response
     */
    public function edit(Uploaduser $uploaduser)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Uploaduser  $uploaduser
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Uploaduser $uploaduser)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Uploaduser  $uploaduser
     * @return \Illuminate\Http\Response
     */
    public function destroy(Uploaduser $uploaduser)
    {
        //
    }
}
