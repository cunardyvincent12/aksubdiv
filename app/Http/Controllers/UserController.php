<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Email;
use App\Uploaduser;
use Illuminate\Support\Facades\Storage;
class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $users = User::all();
        return view('admin.editparticipant',compact('users'));
    }
    public function store(Request $request,$id){
        $users = User::find($id);
        // $users = User::all();
        $emails = Email::where('user_id',$id)->get();
        $uploads = Email::with('uploaduser')->get();
//         $emails->save();

        // dd($upload->uploaduser);
        // $uploads = $emails->uploaduser;
        // Uploaduser::where('email_id',$emails->id)->first();
        // dd($uploads);

        Storage::put('file.jpg', $uploads, 'public');
        return view('admin.participantform',compact('users','emails','id','uploads'));
    }

    public function update(Request $request,$id){
        $users = User::find($id);

        $emails = Email::where('user_id',$id)->get();
        $this->validate($request,[
            'nama'=>'required',
            'email'=>'required|unique:emails',
            'date'=>'required',
            'provience' => 'required',
            'phone'=>'required|min:10|max:12',
            'git'=>'required',
            'line'=>'required|Min:5',
        ]);
$i = 0;
foreach ($emails as $email){
    $email->nama =$request->{'nama'.$i};
        $email->email =$request->{'email'.$i};
        $email->date =$request->{'date'.$i};
        $email->provience =$request->{'provience'.$i};
        $email->address =$request->{'address'.$i};
        $email->phone =$request->{'phone'.$i};
        $email->git =$request->{'git'.$i};
        $email->line =$request->{'line'.$i};
        $email->save();
        // dd($request->email1);
    $i++;

}
        return redirect('admin/'.$id.'/edit');

    }
}
