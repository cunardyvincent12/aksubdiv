<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class payment extends Model
{
    Protected $fillable = [
        'receipt','user_id', 'paid'
    ];
    public function user(){
        $this->belongsTo('App\User');
    }
}
