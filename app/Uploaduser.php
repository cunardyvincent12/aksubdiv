<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Uploaduser extends Model
{
    protected $fillable =[
        'cv','email_id',
    ];
    public function email(){
        $this->belongsTo('App\Email');
    }
}
