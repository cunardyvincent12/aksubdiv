<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
       App\User::create([
           'group'=>'Admin',
           'password'=>bcrypt('adminbncc'),
           'admin'=>'1',
       ]);
    }
}
