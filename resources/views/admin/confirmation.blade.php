<!DOCTYPE html>
<html>
<head>
	<title>Admin Panel</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="css/admin1.css">

</head>
<body>
	<div class="bodi">
		<div class="navbar">
			<div class="sidebar">
				<div class="container">
					<div class="container-kecil">
						<div class="logo">
							<div class="title">
								<h1>BTE</h1>
								<p>Admin Panel</p>
							</div>
						</div>

					<div class="home1">
						<div class="home active" id="js-menu">

						<div class="nav" id="js-menu">
							<div class="navnav">
								<div class="nav-choice nav-active">
								<a href="/home"><img src="assets/AdminPanel/Icon/PaymentConfirmBtn.png">
								</a>
								</div>

								<div class="nav-choice">
								<a href="{{URL('/adminEP')}}"><img src="assets/AdminPanel/Icon/EditDataBtn.png">
								</a>
                                </div>
                                <div class="nav-choice ex">
                                        <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();"><img src="assets/AdminPanel/Icon/AdminLogoutBtn.png">
                                        </a>
                                        </div>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
							</div>
						</div>


						</div>
						<div class="blur active1" id="js-menu1">

						</div>
					</div>

					</div>

				</div>
			</div>


		</div>

		<div class="content">
			<div class="inside-content">
				<div class="title-content">
					<h1>Payment Confirmation</h1>
				</div>
				<div class="body-content">
                @if ($error == 1)
                    <p>Group Have not Paid</p>
                @endif
						@foreach($users as $user)
                    	@if($user->id != 1)
						<div class="data">
							<h2>{{$user->group}}</h2>
							<hr>
							<div>
								<div><p>File: </p></div>
								<div class="data-button">
									<button type="submit" id="{{$user->group}}Btn"><a href="{{ Storage::url($user->payment->receipt ?? '../home/notpaid') ?? 'Not Paid'}}" target="_blank">View</a></button>
									<div id="{{$user->group}}Modal" class="modal">



							</div>
							<form method="POST" action="/admin" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="id" value="{{$user->id}}">
                                <input type="submit" name="paid" value="Verify">
                                </form>
                            </div>
                            <p>Status:
                            </p>
                            <p>@if(empty($user->payment->paid) || $user->payment->paid == 0 )
                                Have Not Paid
                            @else
                                Have paid

                            @endif
                            </p>
							</div>
						</div>
						@endif
                    @endforeach
			</div>

		</div>
	</div>

<script type="text/javascript" src="js/admin1.js"></script>


</body>
</html>
