<!DOCTYPE html>
<html>
<head>
	<title>Admin Panel</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="css/admin2.css">

</head>

<body>
	<div class="bodi">
		<div class="navbar">
			<div class="sidebar">
				<div class="container">
					<div class="container-kecil">
						<div class="logo">
							<div class="title">
								<h1>BTE</h1>
								<p>Admin Panel</p>
							</div>
						</div>

					<div class="home1">
						<div class="home active" id="js-menu">

						<div class="nav" id="js-menu">
							<div class="navnav">
								<div class="nav-choice ">
								<a href="/home"><img src="assets/AdminPanel/Icon/PaymentConfirmBtn.png">
								</a>
								</div>

								<div class="nav-choice nav-active">
								<a href="{{URL('/adminEP')}}"><img src="assets/AdminPanel/Icon/EditDataBtn.png">
								</a>
								</div>
								<div class="nav-choice ex">
                                        <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();"><img src="assets/AdminPanel/Icon/AdminLogoutBtn.png">
                                        </a>
                                        </div>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
							</div>
						</div>


						</div>
						<div class="blur active1" id="js-menu1">

						</div>
					</div>

					</div>

				</div>
			</div>


		</div>

		<div class="content">
			<div class="inside-content">
				<div class="title-content">
					<h1>Edit Participant Data</h1>
				</div>
				<div class="body-content">
					@foreach($users as $user)
						@if($user->id != 1)
						<div class="data-uni">
							<div class="data">
								<h1>{{$user->group}}</h1>
							</div>
							<div class="edit-data">
								<a href="{{action('UserController@store', $user['id'])}}"><button>Edit Data</button></a>
							</div>
						</div>
						@endif
					@endforeach


				</div>
			</div>

		</div>

	</div>


</body>
</html>
