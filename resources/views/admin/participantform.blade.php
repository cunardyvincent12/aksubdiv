<!DOCTYPE html>
<html>
<head>
	<title>Admin Panel</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="../../css/admin3.css">
	<link rel="stylesheet" href="../../css/all.css">
</head>
<body>
	<div class="bodi">
		<div class="navbar">
			<div class="sidebar">
				<div class="container">
					<div class="container-kecil">
						<div class="logo">
							<div class="title">
								<h1>BTE</h1>
								<p>Admin Panel</p>
							</div>
						</div>

					<div class="home1">
						<div class="home active" id="js-menu">

						<div class="nav" id="js-menu">
							<div class="navnav">
								<div class="nav-choice ">
								<a href="/home"><img src="../../assets/AdminPanel/Icon/PaymentConfirmBtn.png">
								</a>
								</div>

								<div class="nav-choice nav-active">
								<a href="{{url('/adminEP')}}"><img src="../../assets/AdminPanel/Icon/EditDataBtn.png">
								</a>
								</div>
								<div class="nav-choice ex">
                                        <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();"><img src="../../assets/AdminPanel/Icon/AdminLogoutBtn.png">
                                        </a>
                                        </div>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
							</div>
						</div>


						</div>
						<div class="blur active1" id="js-menu1">

						</div>
					</div>

					</div>

				</div>
			</div>


		</div>

		<div class="content">
			<div class="inside-content">
            <form method="POST" action="/admin/{{$id}}/redit" enctype="multipart/form-data">
                @csrf
             <div class="title-content">
            <h1>{{$users->find($id)->group}}</h1>
            </div>
            @php
                $i =0;
            @endphp

            @foreach($emails as $email)

            <div class="body-content">
                <div class="middle-content">

                <div class="middle-bio">
                    <div class="md-content">
                        <h1>{{$email->role}}</h1>
                        <div class="middle-data">
                            <div class="middle-divide">
                                <div class="unit-data">
                                    <p>Name</p>
                                    <div class="kotak-middledata">
                                    <input type="text" name="nama{{$i}}"value="{{$email->nama}}">
                                    </div>
                                </div>
                                <div class="unit-data">
                                    <p>Address</p>
                                    <div class="kotak-middledata">
                                    <input type="text" name="address{{$i}}" value="{{$email->address}}">
                                    </div>
                                </div>
                                <div class="unit-data">
                                    <p>Email</p>
                                    <div class="kotak-middledata">
                                    <input type="email" name="email{{$i}}" value="{{$email->email}}">
                                    </div>
                                </div>
                                <div class="unit-data">
                                    <p>Birth Place</p>
                                    <div class="kotak-middledata">
                                            <input type="text" name="provience{{$i}}" value="{{$email->provience}}">
                                    </div>
                                </div>
                            </div>
                            <div class="middle-divide">
                                <div class="unit-data">
                                    <p>WhatsApp Number</p>
                                    <div class="kotak-middledata">
                                            <input type="tel" name="phone{{$i}}" value="{{$email->phone}}">
                                    </div>
                                </div>
                                <div class="unit-data">
                                    <p>Line ID</p>
                                    <div class="kotak-middledata">
                                            <input type="text" name="line{{$i}}" value="{{$email->line}}">
                                    </div>
                                </div>
                                <div class="unit-data">
                                    <p>Gitlab ID</p>
                                    <div class="kotak-middledata">
                                            <input type="text" name="git{{$i}}" value="{{$email->git}}">
                                    </div>
                                </div>
                                <div class="unit-data">
                                    <p>Birth Date</p>
                                    <div class="kotak-middledata">
                                            <input type="date" name="date{{$i}}" value="{{$email->date}}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="garis"></div>
                        <div class="middle-upload">
                            <div class="bawah-garis">

                                <div class="upload-btn-wrapper">
                                <input type="file" name="cv" id="file-upload">
                                <button class="btn" id="file-upload-filename" onclick="fontFunction()"><p id="file">{{$email->uploaduser->cv ?? 'CV Not Found'}}</p></button>

                                </div>
                                <i class="fas fa-folder logo-folder"></i>
                                <h3>|</h3>
                        </div>
                        <button class="viewcv"><a href="{{ Storage::disk('local')->url($email->uploaduser->cv ?? 'CV Not Found')}}"  target="_blank">
                            View CV
                    </a>
                </button>
                        <div id="myModal" class="modal">

                              <!-- Modal content -->
                              <div class="modal-content">
                                <span class="close">&times;</span>
                                <p>Some text in the Modal..</p>
                                <img src="">
                              </div>

                        </div>
                    </div>
                    </div>
                </div>

            </div>
            </div>
            @php
            $i++;
        @endphp
            @endforeach
            <div class="save-data">
            <input type="submit"  value="Save Data">
                <button><a href="{{url('/adminEP')}}">Back</a></button>
            </div>
        </form>

    </div>
	</div>
</div>
<script type="text/javascript" src="../../js/admin3.js"></script>

</body>
</html>
