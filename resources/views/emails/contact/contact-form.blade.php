@component('mail::message')
# Introduction
selamat anda telah mendaftar dengan email
{{$data['email']}}


@component('mail::button', ['url' => ''])
Button Text
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
