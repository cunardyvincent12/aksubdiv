@component('mail::message')
# Introduction

Congratulations you have succed in signin to this competition

@component('mail::button', ['url' => ''])
Register
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
