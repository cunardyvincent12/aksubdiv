<html>
    <head>
	<title>Dashboard</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="css/dbstyle.css">
	<link rel="stylesheet" type="text/css" href="css/all.css">
</head>
<body>
	<div class="bodi">
		<div class="navbar">

			<div class="sidebar height" id="js-h1">
				<div class="container height" id="js-h2">
					<div class="container-kecil height" id="js-h3">
						<div class="logo">
						<div class="menu-icon " id="js-navbar-toggle">
							<div class="line"></div>
							<div class="line"></div>
							<div class="line"></div>
						</div>
						<img src="Dashboard/LogoBar.png"></div>

					<div class="home1 height" id="js-h4">
						<div class="home active" id="js-menu">
						<p>Welcome</p>
                        <h1>{{$users->group}}</h1>

						<div class="nav" id="js-menu">
							<div class="navnav">
								<div class="nav-choice" style="background-color: #54A1AF">
								<a href="/home"><img src="DashBoard/Icon/home-icon-silhouette.png">
								<p>Home</p></a>
								</div>

								<div class="nav-choice">
								<a href="/payment"><img src="DashBoard/Icon/cash.png">
								<p>Payment</p></a>
								</div>
								<div class="nav-choice">
                                        <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();"><img src="DashBoard/Icon/logout.png">

                                         <p>{{ __('Logout') }}</p>
                                        </a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                @csrf
                                            </form>
								</div>
							</div>
							<div class="footer">
							<p>2019 @ Binus Computer Club</p>
						</div>
						</div>


						</div>
						<div class="blur active1" id="js-menu1">

						</div>
					</div>

					</div>

				</div>

			</div>


		</div>

		<div class="content">
			<div class="head-title">
				<div class="title">
					<h1>{{$users->group}}</h1>
				</div>

			</div>
			<div class="middle">
				<div class="middle-content">
						@if (session('status'))
						<div class="alert alert-success" role="alert">
							{{ session('status') }}
						</div>
					@endif
					@php
					 $i = 0;
					@endphp
					@foreach($emails as $email)
					<div class="middle-bio">
						<div class="md-content">
							<h1>{{$email->role}}</h1>
							<div class="middle-data">
								<div class="middle-divide">
									<div class="unit-data">
										<p>Name</p>
										<div class="kotak-middledata">
										<p>{{$email->nama}}</p>
										</div>
									</div>
									<div class="unit-data">
										<p>Address</p>
										<div class="kotak-middledata">
										<p>{{$email->address}}</p>
										</div>
									</div>
									<div class="unit-data">
										<p>Email</p>
										<div class="kotak-middledata">
										<p>{{$email->email}}</p>
										</div>
									</div>
									<div class="unit-data">
										<p>Birth Place</p>
										<div class="kotak-middledata">
										<p>{{$email->provience}}</p>
										</div>
									</div>
								</div>
								<div class="middle-divide">
									<div class="unit-data">
										<p>WhatsApp Number</p>
										<div class="kotak-middledata">
										<p>{{$email->phone}}</p>
										</div>
									</div>
									<div class="unit-data">
										<p>Line ID</p>
										<div class="kotak-middledata">
										<p>{{$email->line}}</p>
										</div>
									</div>
									<div class="unit-data">
										<p>Gitlab ID</p>
										<div class="kotak-middledata">
										<p>{{$email->git}}</p>
										</div>
									</div>
									<div class="unit-data">
										<p>Birth Date</p>
										<div class="kotak-middledata">
										<p>{{$email->date}}</p>
										</div>
									</div>
								</div>
							</div>
                            <div class="garis"></div>
                            <form method="POST" action="/home{{$i}}" enctype="multipart/form-data">
							<div class="middle-upload">

								@csrf
								<div class="bawah-garis">

									<div class="upload-btn-wrapper">
                                    <input type="file" name="cv{{$i}}" id="file-upload{{$i}}" placeholder="Upload ID Card" required>
                                    <button class="btn" id="file-upload-filename{{$i}}" onclick=""><p id="file">Upload CV</p></button>

									</div>
									<i class="fas fa-folder logo-folder"></i>
									<h3>|</h3>
							</div>
							<input type="number" name="id" style="display:none;" value="{{$email->id}}">
							<input type="submit">
                            </div>
                        </form>
                        @php
                                $i = $i + 1;
                                @endphp
						</div>
					</div>
            		@endforeach

				</div>
			</div>
			<div class="contact">
				<div class="inside-contact">
					<div class="C-kiri">
						<p>Jika ada informasi data diri yang ingin diubah. Silakan hubungi Contact Person berikut:</p>
					</div>
					<div class="C-kiri">
						<div class="C-line">
							<p>Andi Saputra</p>
							<span class="sosmed">
							<img src="DashBoard/Icon/line.png"><p>AndiSpt</p>	</span>
							<span class="sosmed"><img src="{{url('assets/DashBoard/Icon/whatsapp.png')}}><p>0822 6512 3212</p></span>
						</div>
						<div class="C-line">
							<p>Marta</p>
							<span class="sosmed">
							<img src="DashBoard/Icon/line.png"><p>Mr T</p></span>
							<span class="sosmed"><img src="{{url('assets/DashBoard/Icon/whatsapp.png')}}"><p>0852 8819 0912</p></span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


	<script src="js/db.js"></script>
</body>
</html>
