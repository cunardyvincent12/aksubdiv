<!DOCTYPE html>
<html>
<head>
	<title>Admin Panel</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="../../css/admin1.css">
    <link rel="stylesheet" type="text/css" href="../../css/admin3.css">
</head>
<body>
	<div class="bodi">
		<div class="navbar">
			<div class="sidebar">
				<div class="container">
					<div class="container-kecil">
						<div class="logo">
							<div class="title">
								<h1>BTE</h1>
								<p>Admin Panel</p>
							</div>
						</div>

					<div class="home1">
						<div class="home active" id="js-menu">

						<div class="nav" id="js-menu">
							<div class="navnav">
								<div class="nav-choice nav-active">
								<a href="/home"><img src="../../AdminPanel/Icon/PaymentConfirmBtn.png">
								</a>
								</div>

								<div class="nav-choice">
								<a href="{{URL('/adminEP')}}"><img src="../../AdminPanel/Icon/EditDataBtn.png">
								</a>
								</div>
								<div class="nav-choice ex">
								<a href="{{ route('logout') }}" onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();"><img src="../../AdminPanel/Icon/AdminLogoutBtn.png">
								</a>
                                </div>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
							</div>
						</div>


						</div>
						<div class="blur active1" id="js-menu1">

						</div>
					</div>

					</div>

				</div>
			</div>


        </div>

        <main class="mainClass">
                @yield('admin')
            </main>

	</div>






</body>
</html>
