<!DOCTYPE html>
<html>
<head>
	<title>Dashboard</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="css/dbstyle.css">

</head>
<body>
	<div class="bodi">
		<div class="navbar">

			<div class="sidebar">
				<div class="container">
					<div class="container-kecil">
						<div class="logo">
						<div class="menu-icon " id="js-navbar-toggle">
							<div class="line"></div>
							<div class="line"></div>
							<div class="line"></div>
						</div>
						<img src="Dashboard/LogoBar.png"></div>

                    @guest
                    @if(Route::has('register'))
                    @endif
                    @else
					<div class="home1">
						<div class="home active" id="js-menu">
						<p>Welcome</p>
						<h1>{{$users->group}}</h1>

						<div class="nav" id="js-menu">
							<div class="navnav">
								<div class="nav-choice" style="background-color: #54A1AF">
								<a href="/home"><img src="DashBoard/Icon/home-icon-silhouette.png">
								<p>Home</p></a>
								</div>

								<div class="nav-choice">
								<a href="/payment"><img src="DashBoard/Icon/cash.png">
								<p>Payment</p></a>
								</div>
								<div class="nav-choice">
								<a href="{{ route('logout') }}" onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();"><img src="DashBoard/Icon/logout.png">

                                  {{ __('Logout') }}
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
								</div>
							</div>
						</div>


						</div>
						<div class="blur active1" id="js-menu1">

						</div>
					</div>
					<div class="footer">
							<p>2019 @ Binus Computer Club</p>
						</div>
                    </div>
                    @endguest

				</div>
			<!-- <ul>
				<li> <a class="active" href="#home">Home</a> </li>
				<li> <a href="#news">News</a> </li>
				<li> <a href="#news">News</a> </li>
			</ul> -->
			</div>


        </div>
        <main class="py-4">
            @yield('isi')
        </main>

	</div>



	<script>
let mainNav = document.getElementById('js-menu');
let mainNav1 = document.getElementById('js-menu1');
let navBarToggle = document.getElementById('js-navbar-toggle');

navBarToggle.addEventListener('click', function () {

    mainNav.classList.toggle('active');
    mainNav1.classList.toggle('active1');

})



</script>
</body>
</html>
