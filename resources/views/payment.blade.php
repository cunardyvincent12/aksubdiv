<html>
<head>
	<title>Dashboard</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="css/dbstyle.css">
	<link rel="stylesheet" type="text/css" href="css/dbpstyle.css">
	<link rel="stylesheet" type="text/css" href="css/timeline.css">
	<link rel="stylesheet" href="css/all.css">

</head>
<body>
	<div class="bodi">
		<div class="navbar ">
			<div class="sidebar ">
				<div class="container  ">
					<div class="container-kecil   ">
						<div class="logo">
						<div class="menu-icon " id="js-navbar-toggle">
							<div class="line"></div>
							<div class="line"></div>
							<div class="line"></div>
						</div>
						<img src="Dashboard/LogoBar.png"></div>

					<div class="home1">
						<div class="home active" id="js-menu">
						<p>Welcome</p>
						<h1>{{$users->group}}</h1>

						<div class="nav" id="js-menu">
							<div class="navnav">
								<div class="nav-choice">
								<a href="/home"><img src="assets/DashBoard/Icon/home-icon-silhouette.png">
								<p>Home</p></a>
								</div>

								<div class="nav-choice nav-active">
								<a href="/payment"><img src="assets/DashBoard/Icon/cash.png">
								<p>Payment</p></a>
								</div>
								<div class="nav-choice">
                                        <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();"><img src="assets/DashBoard/Icon/logout.png">
                                        <p>Logout</p></a>
                                        </div>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
							</div>
							<div class="footer">
							<p>2019 @ Binus Computer Club</p>
						</div>
						</div>


						</div>
						<div class="blur active1" id="js-menu1">

						</div>
					</div>

					</div>

				</div>
			</div>


		</div>

		<div class="content">
			<div class="head-title">
				<div class="title">
					<h1>Payment</h1>
				</div>

			</div>
			<div class="middle">
				<div class="middle-content">
					<div class="middle-bio">
						<div class="md-content mpctn">

							<p>Payment Method Instructions</p>
							<div class="md-payment">
								<div class="steps">
									<div class="title-steps">
										<p>1. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do </p>
									</div>
									<div class="timeline hidden" id="js-hidden">
 									 	<div class="container-tm right">
    									<div class="content-tm">
      										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do</p>
    									</div>
 									 	</div>
  										<div class="container-tm right">
    									<div class="content-tm">
      										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do</p>
    									</div>
 										</div>
 										<div class="container-tm right">
    									<div class="content-tm">
      										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do</p>
    									</div>
 										</div>
 										<div class="container-tm right">
    									<div class="content-tm">
      										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do</p>
    									</div>
 										</div>
 										<div class="container-tm right">
    									<div class="content-tm">
      										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do</p>
    									</div>
 										</div>
 										<div class="container-tm right">
    									<div class="content-tm">
      										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do</p>
    									</div>
 										</div>

									</div>

									<div class="gr-steps " id="js-plus">
										<hr>
										<i class="fas fa-plus front" id="plus"></i>
									</div>
								</div>
								<div class="steps">
									<div class="title-steps">
										<p>2. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do </p>
									</div>
									<div class="timeline hidden1" id="js-hidden1">
 									 	<div class="container-tm right">
    									<div class="content-tm">
      										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do</p>
    									</div>
 									 	</div>
  										<div class="container-tm right">
    									<div class="content-tm">
      										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do</p>
    									</div>
 										</div>
 										<div class="container-tm right">
    									<div class="content-tm">
      										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do</p>
    									</div>
 										</div>
 										<div class="container-tm right">
    									<div class="content-tm">
      										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do</p>
    									</div>
 										</div>
 										<div class="container-tm right">
    									<div class="content-tm">
      										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do</p>
    									</div>
 										</div>
 										<div class="container-tm right">
    									<div class="content-tm">
      										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do</p>
    									</div>
 										</div>

									</div>
									<div class="gr-steps" id="js-plus1" >
										<hr>
										<i class="fas fa-plus front" id="plus1"></i>
									</div>
								</div>
							</div>



						</div>
					</div>
					<div class="middle-bio midpay front" id="js-upload">
@if($stat != 1)
						<form method="post" action="{{url('/payment')}}" enctype="multipart/form-data">
							@csrf
						<div class="md-content mpctn upay">
							<p>Upload Payment Receipt</p>
							<div class="middle-data">
								<div class="upload-btn-wrapper">
									<input type="file" name="receipt" id="file-upload">
									<button class="btn">Upload</button><span>your payment receipt here !!</span>
								</div>

							</div>
							<div class="f-up-pay" id="file-upload-filename" onclick="fontFunction()">
                                <p id="filename">File Name: <span>-</span></p>
                            </div>
                                <input class="btn" type="submit" value="Submit">
@else
                                <p style="font-family: MonBold; text-align:center;"> Your Payment has been Verified !  </p>
                                @endif</span>





						</div>
					</div>
					</div>

				</div>
			</div>

		</div>
	</div>





	<script src="js/db2.js"></script>
</body>
</html>
