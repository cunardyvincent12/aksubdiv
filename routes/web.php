<?php
use App\Http\Controllers\PaymentController;
use App\Payment;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/','ContactController@index')->name('welcome');
Route::post('/','ContactController@store');
Auth::routes();
// Route::get('/home','EmailController@index');
// Route::post('/register','UserController@store');
Route::get('/coba', 'ContactController@index');
Route::post('/coba', 'ContactController@store');
Route::get('/payment','PaymentController@index');
Route::post('/payment','PaymentController@store');
Route::post('/home0','UploaduserController@store');
Route::post('/home1','UploaduserController@store1');
Route::post('/home2','UploaduserController@store2');
Route::post('/admin','PaymentController@pay');
Route::get('/adminEP','UserController@index');
Route::get('/admin/{id}/edit','UserController@store');
Route::post('/admin/{id}/redit','UserController@update');
Route::get('/home/notpaid','PaymentController@check');
Route::group(['middleware' =>['web','auth']],function(){
    Route::get('/home', function () {
        return redirect('/home');
    });
    Route::get('/home','EmailController@index');
});




